def generate_numbers(number):
    yield number
    while number != 1:
        if number % 2 == 0:
            number = number // 2
        else:
            number = number * 3 + 1
        yield number


n = int(input())
print(' '.join(map(str, generate_numbers(n))))
