def number(row, col):
    if row == 1 and col == 1:
        return 1

    if row > col:
        if row % 2 == 0:
            row_x = row ** 2
            return row_x - col + 1
        row_x = (row - 1) ** 2 + 1
        return row_x + col - 1

    if col % 2 == 0:
        col_x = (col - 1) ** 2 + 1
        return col_x + row - 1

    col_x = col ** 2
    return col_x - row + 1


tests = int(input())
for _ in range(tests):
    y, x = [int(i) for i in input().split(' ')]
    print(number(y, x))
