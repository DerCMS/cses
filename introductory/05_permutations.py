n = int(input())

if 2 <= n <= 3:
    print('NO SOLUTION')
else:
    l_1 = [str(i) for i in range(2, n + 1, 2)]
    l_2 = [str(i) for i in range(1, n + 1, 2)]
    print(' '.join(l_1 + l_2))
