def find_longest_sub_sequence_count(sequence):
    longest = 0
    current = 0
    last_letter = ''
    for letter in sequence:
        if letter == last_letter:
            current += 1
        else:
            current = 1
            last_letter = letter
        longest = max(longest, current)

    return longest


dna_sequence = input()
print(find_longest_sub_sequence_count(dna_sequence))
