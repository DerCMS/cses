n_1 = int(input())
n_2 = [int(i) for i in input().split(' ')]
sum_1 = sum(range(n_1 + 1))
sum_2 = sum(n_2)

print(str(sum_1 - sum_2))
