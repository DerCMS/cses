n = int(input())
x = [int(i) for i in input().split(' ')]


last = 0
turns_needed = 0

for number in x:
    if number < last:
        diff = last - number
        turns_needed += diff
    else:
        last = number

print(turns_needed)
