def sum_up(n):
    return (n * (n + 1)) // 2


field_size = int(input())

print('\n'.join(
    str(sum_up(k ** 2 - 1) - sum_up(k - 2) * 8)
    for k in range(1, field_size + 1))
)
